#stage 1
FROM golang:alpine AS system
RUN apk update && apk add curl

#stage 2
FROM golang:alpine AS build
RUN apk update && apk add git
WORKDIR /app
COPY go.mod /app/go.mod
RUN go mod download
COPY . /app
RUN go build -o main .

#stage 3
FROM scratch AS app
COPY --from=system /usr/bin /usr/bin
COPY --from=build /app/main /app/main
COPY --from=build /app/views /views
COPY --from=build /app/.env /.env
EXPOSE $PORT
ENTRYPOINT ["/app/main"]
